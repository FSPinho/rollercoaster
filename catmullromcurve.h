#ifndef CATMULLROMCURVE_H
#define CATMULLROMCURVE_H

#include "abscurve.h"
#include "matrixes.h"

class CatmullRomCurve : public AbsCurve {
public:
    CatmullRomCurve(Vector<Vector<float> > controlPoints);
    CatmullRomCurve(Vector<ControlObjects *> controlObjects);
};

#endif // CATMULLROMCURVE_H
