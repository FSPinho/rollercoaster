#ifndef VECTOR_H
#define VECTOR_H

#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <cmath>

using namespace std;

template<class T>
class Vector {
public:
    Vector();
    Vector(initializer_list<T>);
    ~Vector();

    void setX(T x);
    void setY(T y);
    void setZ(T z);

    T getX();
    T getY();
    T getZ();

    T& operator [](int index);
    Vector<T>& operator <<(T other);
    bool operator ==(Vector<T> other);
    bool operator !=(Vector<T> other);
    Vector<T>& operator =(Vector<T> other);
    Vector<T>& operator +(Vector<T>& other);
    Vector<T>& operator -(Vector<T>& other);
    Vector<T>& operator *(Vector<T>& other);
    Vector<T>& operator +(T other);
    Vector<T>& operator *(T other);
    Vector<T>& operator /(T other);

    int getSize();
    string toString();
    void normalize();
    void clear();

private:
    vector<T> contentVector;
};

template<class T>
Vector<T>::Vector() {}

template<class T>
Vector<T>::Vector(initializer_list<T> other) {
    for(auto i = other.begin(); i != other.end(); i++)
        (*this) << *i;
}

template<class T>
Vector<T>::~Vector() {

}

template<class T>
void Vector<T>::setX(T x) { (*this)[0] = x; }

template<class T>
void Vector<T>::setY(T y) { (*this)[1] = y; }

template<class T>
void Vector<T>::setZ(T z) { (*this)[2] = z; }

template<class T>
T Vector<T>::getX() { return (*this)[0]; }

template<class T>
T Vector<T>::getY() { return (*this)[1]; }

template<class T>
T Vector<T>::getZ() { return (*this)[2]; }

template<class T>
T &Vector<T>::operator [](int index) {
    if(index >= contentVector.size())
        contentVector.resize(index + 1);

    return contentVector[index];
}

template<class T>
Vector<T>& Vector<T>::operator <<(T other) {
    Vector<T>& ref = *(this);
    ref[contentVector.size()] = other;
    return ref;
}

template<class T>
bool Vector<T>::operator ==(Vector<T> other) {
    if(getSize() != other.getSize())
        return false;

    for(int i = 0; i < other.getSize(); i++) {
        if((*this)[i] != other[i])
            return false;
    }

    return true;
}

template<class T>
bool Vector<T>::operator !=(Vector<T> other) {
    return !((*this) == other);
}

template<class T>
Vector<T>& Vector<T>::operator =(Vector<T> other) {
    contentVector.clear();
    for(int i = 0; i < other.getSize(); i++)
        (*this)[i] = other[i];

    return *this;
}

template<class T>
Vector<T>& Vector<T>::operator +(Vector<T>& other) {
    Vector<T>* newVector = new Vector<T>;

    if(getSize() == other.getSize()) {
        for(int i = 0; i < getSize(); i++)
            (*newVector)[i] = (*this)[i] + other[i];
    }

    return *newVector;
}

template<class T>
Vector<T>& Vector<T>::operator -(Vector<T>& other) {
    Vector<T>* newVector = new Vector<T>;

    if(getSize() == other.getSize()) {
        for(int i = 0; i < getSize(); i++)
            (*newVector)[i] = (*this)[i] - other[i];
    }

    return *newVector;
}

template<class T>
Vector<T>& Vector<T>::operator *(Vector<T>& other) {
    Vector<T>* newVector = new Vector<T>;

    if(getSize() == 3 && other.getSize() == 3) {
        newVector->setX(getY() * other.getZ() - other.getY() * getZ());
        newVector->setY(getZ() * other.getX() - other.getZ() * getX());
        newVector->setZ(getX() * other.getY() - other.getX() * getY());
    }


    return *newVector;
}

template<class T>
Vector<T>& Vector<T>::operator +(T other) {
    Vector<T>* newVector = new Vector<T>;

    for(int i = 0; i < getSize(); i++)
        (*newVector)[i] = (*this)[i]+other;

    return *newVector;
}

template<class T>
Vector<T>& Vector<T>::operator *(T other) {
    Vector<T>* newVector = new Vector<T>;

    for(int i = 0; i < getSize(); i++)
        (*newVector)[i] = (*this)[i]*other;

    return *newVector;
}

template<class T>
Vector<T>& Vector<T>::operator /(T other) {
    Vector<T>* newVector = new Vector<T>;

    for(int i = 0; i < getSize(); i++)
        (*newVector)[i] = (*this)[i]/other;

    return *newVector;
}

template<class T>
int Vector<T>::getSize() {
    return contentVector.size();
}

template<class T>
string Vector<T>::toString() {
    stringstream desc;

    desc << "[";
    for(int i = 0; i < contentVector.size(); i++) {
        desc << contentVector[i] << (i == contentVector.size() - 1? "" : ", ");
    }
    desc << "]";

    return desc.str();
}

template<class T>
void Vector<T>::normalize() {
    if(getX() != 0.0 || getY() != 0.0 || getZ() != 0.0) {
        T n = sqrt(pow(getX(), 2) + pow(getY(), 2) + pow(getZ(), 2));
        (*this) = (*this) / n;
    }
}

template<class T>
void Vector<T>::clear() {
    contentVector.clear();
}

template<class T>
std::ostream& operator<<(std::ostream& o, Vector<T>& v){
  return o << (v.toString().c_str());
}

#endif // VECTOR_H
