#include "beziercurve.h"

BezierCurve::BezierCurve(Vector<Vector<float> > points) : AbsCurve(points) {
    Matrix<float> m = MATRIX_BEZIER;
    setM(m);
}

BezierCurve::BezierCurve(Vector<ControlObjects *> controlObjects) : AbsCurve(controlObjects) {
    Matrix<float> m = MATRIX_BEZIER;
    setM(m);
}

void BezierCurve::onDisplay() {
    AbsCurve::onDisplay();
    glBegin(GL_LINES);
    glColor3f(0, 0, 0);

    Vector<float> p0 = getControlPoints()[0];
    Vector<float> p1 = getControlPoints()[1];
    Vector<float> p2 = getControlPoints()[2];
    Vector<float> p3 = getControlPoints()[3];

    glVertex3f(p0.getX(), p0.getY(), p0.getZ());
    glVertex3f(p1.getX(), p1.getY(), p1.getZ());
    glVertex3f(p2.getX(), p2.getY(), p2.getZ());
    glVertex3f(p3.getX(), p3.getY(), p3.getZ());

    glEnd();
}
