#ifndef MATRIX
#define MATRIX

#include "vector.h"
#include <string>
#include <cmath>

using namespace std;

template<class T>
class Matrix {
public:
    Matrix();
    Matrix(initializer_list< initializer_list<T> >);

    Matrix<T>& operator <<(Vector<T>);
    Vector<T>& operator [](int);
    bool operator ==(Matrix<T>& other);
    bool operator !=(Matrix<T>& other);
    Matrix<T>& operator =(initializer_list< Vector<T> >);
    Matrix<T>& operator +(Matrix<T>& other);
    Matrix<T>& operator -(Matrix<T>& other);
    Matrix<T>& operator *(Matrix<T>& other);
    Matrix<T>& operator *(T other);

    int getWidth();
    int getHeight();
    Matrix<T>& getTranspose();
    string toString();
    const T* toVector();
    void clear();

private:
    Vector< Vector<T> > contentMatrix;
};

template<class T>
Matrix<T>::Matrix() {}

template<class T>
Matrix<T>::Matrix(initializer_list< initializer_list<T> > other) {
    int indexI = 0, indexJ = 0;
    for(auto i = other.begin(); i != other.end(); i++, indexI++) {
        indexJ = 0;
        for(auto j = (*i).begin(); j != (*i).end(); j++, indexJ++) {
            (*this)[indexI][indexJ] = *j;
        }
    }
}

template<class T>
Matrix<T>& Matrix<T>::operator <<(Vector<T> other) {
    (*this)[contentMatrix.getSize()] = other;

    return *this;
}

template<class T>
Vector<T>& Matrix<T>::operator [](int index) {
    return contentMatrix[index];
}

template<class T>
bool Matrix<T>::operator ==(Matrix<T>& other) {
    if(getHeight() != other.getHeight() || getWidth() != other.getWidth())
        return false;

    for(int i = 0; i < getHeight(); i++)
        if(contentMatrix[i] != other[i])
            return false;

    return true;
}

template<class T>
bool Matrix<T>::operator !=(Matrix<T>& other) {
    return !((*this) == other);
}

template<class T>
Matrix<T>& Matrix<T>::operator =(initializer_list< Vector<T> > other) {
    for(auto i = other.begin(); i != other.end(); i++) {
        (*this) << *i;
    }

    return *this;
}

template<class T>
Matrix<T>& Matrix<T>::operator +(Matrix<T>& other) {
    Matrix<T>* newMatrix = new Matrix<T>;

    if(getWidth() == other.getWidth() && getHeight() == other.getHeight()) {
        for(int i = 0; i < getHeight(); i++) {
            for(int j = 0; j < getWidth(); j++) {
                (*newMatrix)[i][j] = (*this)[i][j] + other[i][j];
            }
        }
    }

    return *newMatrix;
}

template<class T>
Matrix<T>& Matrix<T>::operator -(Matrix<T>& other) {
    Matrix<T>* newMatrix = new Matrix<T>;

    if(getWidth() == other.getWidth() && getHeight() == other.getHeight()) {
        for(int i = 0; i < getHeight(); i++) {
            for(int j = 0; j < getWidth(); j++) {
                (*newMatrix)[i][j] = (*this)[i][j] - other[i][j];
            }
        }
    }

    return *newMatrix;
}

template<class T>
Matrix<T>& Matrix<T>::operator *(Matrix<T>& other) {
    Matrix<T>* newMatrix = new Matrix<T>;

    if(getWidth() == other.getHeight()) {
        for(int i = 0; i < getHeight(); i++) {
            for(int j = 0; j < other.getWidth(); j++) {
                (*newMatrix)[i][j] = 0;
                for(int k = 0; k < getWidth(); k++) {
                    (*newMatrix)[i][j] += (*this)[i][k] * other[k][j];
                }
            }
        }
    }

    return *newMatrix;
}

template<class T>
Matrix<T>& Matrix<T>::operator *(T other) {
    Matrix<T>* newMatrix = new Matrix<T>;

    for(int i = 0; i < getHeight(); i++) {
        for(int j = 0; j < getWidth(); j++) {
            (*newMatrix)[i][j] = (*this)[i][j] * other;
        }
    }

    return *newMatrix;
}

template<class T>
int Matrix<T>::getWidth() {
    return contentMatrix[0].getSize();
}

template<class T>
int Matrix<T>::getHeight() {
    return contentMatrix.getSize();
}

template<class T>
Matrix<T>& Matrix<T>::getTranspose() {
    Matrix<T>* newMatrix = new Matrix<T>;

    for(int i = 0; i < getHeight(); i++) {
        for(int j = 0; j < getWidth(); j++) {
            (*newMatrix)[j][i] = (*this)[i][j];
        }
    }

    return *newMatrix;
}

template<class T>
string Matrix<T>::toString() {
    stringstream desc;
    for(int i = 0; i < contentMatrix.getSize(); i++) {
        desc << "|";
        for(int j = 0; j < contentMatrix[i].getSize(); j++) {
            desc << contentMatrix[i][j] << (j == contentMatrix[i].getSize() - 1? "": " ");
        }
        desc << "|\n";
    }

    return desc.str();
}

template<class T>
const T* Matrix<T>::toVector() {
    T* vector = new T[getWidth() * getHeight()];
    for(int i = 0; i < getHeight(); i++)
        for(int j = 0; j < getWidth(); j++)
            vector[i*getWidth() + j] = (*this)[i][j];
    return vector;
}

template<class T>
void Matrix<T>::clear() {
    for(int i = 0; i < getHeight(); i++)
        contentMatrix[i].clear();

    contentMatrix.clear();

}

template<class T>
std::ostream& operator<<(std::ostream& o, Matrix<T>& m){
  return o << (m.toString().c_str());
}

#endif // MATRIX

