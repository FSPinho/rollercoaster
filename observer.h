#ifndef OBSERVER_H
#define OBSERVER_H


class Observer {
public:
    virtual void notifyChanged() = 0;
};

#endif // OBSERVER_H
