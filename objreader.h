#ifndef OBJREADER_H
#define OBJREADER_H

#include <string>
#include <fstream>
#include <iostream>
#include "vector.h"
#include "controlobjects.h"

using namespace std;

class ObjReader : public Object3D {
public:
    ObjReader(string filePath);
    Vector<ControlObjects*> getControlObjects();

    void onDisplay();
    void onDisplaySelected();
    void onDisplayShadow();

private:
    Vector<ControlObjects*> controllObjects;
};

#endif // OBJREADER_H
