#include "objreader.h"

ObjReader::ObjReader(string filePath) {
    fstream file(filePath);

    string header;
    float scale = 5.0f;
    while(file >> header) {
        if(header.compare("v") == 0) {
            float x, y, z;
            file >> x >> y >> z;
            ControlObjects* c = new ControlObjects(x*scale, y*scale, z*scale);
            controllObjects << c;
        } else if(header.compare("v") == 0) {
            float x, y, z;
            file >> x >> y >> z;
            ControlObjects* c = new ControlObjects(x*scale, y*scale, z*scale);
            controllObjects << c;
        }
    }

    cout << controllObjects.getSize() << endl;
}

Vector<ControlObjects *> ObjReader::getControlObjects() {
    return controllObjects;
}

void ObjReader::onDisplay() {

}

void ObjReader::onDisplaySelected() {

}

void ObjReader::onDisplayShadow() {

}
