#ifndef CAMERA_H
#define CAMERA_H

#include <cmath>
#include "object3d.h"
#include "matrix.h"

#define MODE_PESPECTIVE 0
#define MODE_ORTOGRPHIC 1

#define POINT_DEFAULT 0
#define POINT_TOP 1
#define POINT_DOWN 2
#define POINT_FRONT 3
#define POINT_BACK 4
#define POINT_LEFT 5
#define POINT_RIGHT 6

class Camera : public Object3D {
public:
    Camera(GLdouble ex, GLdouble ey, GLdouble ez,\
           GLdouble cx, GLdouble cy, GLdouble cz,\
           GLdouble ux, GLdouble uy, GLdouble uz);

    void onDisplay();
    void onDisplaySelected();
    void onDisplayShadow();

    void setViewMode(int mode, int point);
    void updateViewMode();
    void setAspect(float aspect);

    void rotate(float angle, float x, float y, float z);
    void rotateVertical(float angle);
    void rotateHorizontal(float angle);
    void zoom(float intensity);

    void setEyePosition(float x, float y, float z);
    void setEyePosition(Vector<Matrix<float> > transform);
    void setCenterPosition(float x, float y, float z);
    void setCenterPosition(Vector<Matrix<float> > transform);
    void setUpPosition(float x, float y, float z);
    void setUpPosition(Vector<Matrix<float> > transform);

    float getEyeX() const;
    void setEyeX(float value);

    float getEyeY() const;
    void setEyeY(float value);

    float getEyeZ() const;
    void setEyeZ(float value);

    float getCenterX() const;
    void setCenterX(float value);

    float getCenterY() const;
    void setCenterY(float value);

    float getCenterZ() const;
    void setCenterZ(float value);

    float getUpX() const;
    void setUpX(float value);

    float getUpY() const;
    void setUpY(float value);

    float getUpZ() const;
    void setUpZ(float value);

private:
    float eyeX, eyeY, eyeZ;
    float centerX, centerY, centerZ;
    float upX, upY, upZ;

    float top, bottom, left, right;

    int viewMode;
    GLfloat angle;
    GLfloat fAspect;

    Matrix<float> getMatrixEyePosition();
    Matrix<float> getMatrixCenterPosition();
    Matrix<float> getMatrixUpPosition();
};

#endif // CAMERA_H
