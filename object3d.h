#ifndef OBJECT3D_H
#define OBJECT3D_H

#include <GL/gl.h>
#include <GL/glut.h>
#include <iostream>
#include <vector.h>
#include <matrix.h>

using namespace std;

class Object3D {
public:
    Object3D();
    Object3D(GLdouble x, GLdouble y, GLdouble z);
    ~Object3D();

    static void displayCube(GLdouble x, GLdouble y, GLdouble z, GLdouble width, GLdouble height, GLdouble depth);
    static void displayCylinder(GLdouble x, GLdouble y, GLdouble z, GLdouble width, GLdouble height, GLdouble depth);
    static void displaySphere(GLdouble x, GLdouble y, GLdouble z, GLdouble width, GLdouble height, GLdouble depth);

    int getId();

    bool getSelected() const;
    void setSelected(bool value);

    bool getSelectable() const;
    void setSelectable(bool value);

    void translate(GLdouble x, GLdouble y, GLdouble z);
    void rotate(GLdouble angle, GLdouble x, GLdouble y, GLdouble z);
    void scale(GLdouble x, GLdouble y, GLdouble z);
    void scale(GLdouble scale);

    Vector<float> getPosition();

    void display();
    void displaySelected();
    void displayShadow();
    virtual void onDisplay() = 0;
    virtual void onDisplaySelected() = 0;
    virtual void onDisplayShadow() = 0;

    GLdouble getX() const;
    void setX(const GLdouble &value);

    GLdouble getY() const;
    void setY(const GLdouble &value);

    GLdouble getZ() const;
    void setZ(const GLdouble &value);

    void setTransforms(Vector<Matrix<float> > transforms);

protected:
    virtual void onObjectChanged();

private:
    static int ID_OBJECT;

    int id;

    GLdouble xt, yt, zt;
    GLdouble xr, yr, zr;
    GLdouble xs, ys, zs;
    Vector<Matrix<float> > transforms;

    bool selectable;
    bool selected;

    void applyTransforms();

};

#endif // OBJECT3D_H
