#ifndef CUBICCURVE_H
#define CUBICCURVE_H

#include <iostream>
#include "abscurve.h"

using namespace std;

class CubicCurve : public AbsCurve {
public:
    CubicCurve(Vector< Vector<float> > controlpoints);
    CubicCurve(Vector<ControlObjects *> controlObjects);
};

#endif // CUBICCURVE_H
