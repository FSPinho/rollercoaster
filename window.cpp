#include "window.h"

Window* currentInstance;

static void displayCallBack() {
    currentInstance->display();
}

static void reshapeCallBack(GLsizei w, GLsizei h) {
    currentInstance->reshape(w, h);
}

static void processKeysCallBack(int key, int x, int y) {
    currentInstance->processKeys(key, x, y);
}

static void processKeysCharCallBack(unsigned char key, int x, int y) {
    currentInstance->processKeysChar(key, x, y);
}

static void processMouseCallBack(int button, int state, int x, int y) {
    currentInstance->processMouse(button, state, x, y);
}

static void processMouseMotionCallBack(int x, int y) {
    currentInstance->processMuseMotion(x, y);
}

static void processMouseMotionPassiveCallBack(int x, int y) {
    currentInstance->processMuseMotionPassive(x, y);
}

static void processPickingCallBack(GLint, GLint, int , int) {

}

Window::Window(int width, int height, string title, int nViews) {
    currentInstance = this;

    this->width = width;
    this->height = height;

    viewAngleX = 0.0f;
    viewAngleY = 0.0f;

    flagMouseDownRotate = false;
    flagMouseDownMoved = false;
    mouseX = mouseY = 0;

    lightX = 50;
    lightY = 50;
    lightZ = 50;

    resizingView = false;
    moovingEnabled = false;
    rotateEnabled = false;
    scaleEnabled = false;
    keyXEnabled = false;
    keyYEnabled = false;
    keyZEnabled = false;

    nViews = nViews < 1? 1: nViews;
    view = 0;

    for(int i = 0; i < nViews; i++) {
        ViewPort* view = new ViewPort();
        addView(view);
    }

    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(width, height);
    glutInitWindowPosition(0, 0);
    glutCreateWindow(title.c_str());

    glutDisplayFunc(displayCallBack);
    glutReshapeFunc(reshapeCallBack);
    glutSpecialFunc(processKeysCallBack);
    glutKeyboardFunc(processKeysCharCallBack);
    glutMouseFunc(processMouseCallBack);
    glutMotionFunc(processMouseMotionCallBack);
    glutPassiveMotionFunc(processMouseMotionPassiveCallBack);

    init();
}

void Window::display() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);

    /* Display objects */
    int aux = 0;
    for(auto i = views.begin(); i != views.end(); i++) {
        ViewPort* view = (*i);
        view->display();

        for(vector<Object3D*>::iterator i = objects.begin(); i != objects.end(); i++) {
            (*i)->display();
            aux++;
        }
    }
    glFlush();
    glutSwapBuffers();
}

void Window::displayToSelect(GLint cursorX, GLint cursorY, int w, int h, int BUFSIZE, GLuint selectBuf[]) {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);

    /* Display objects */
    int aux = 0;
    for(auto i = views.begin(); i != views.end(); i++) {
        ViewPort* view = (*i);
        view->displayToSelect(cursorX, cursorY, w, h, BUFSIZE, selectBuf);

        for(vector<Object3D*>::iterator i = objects.begin(); i != objects.end(); i++) {
            glPushName((*i)->getId());
            (*i)->display();
            glPopName();
            aux++;
        }
    }
    glFlush();
    glutSwapBuffers();
}

void Window::reshape(GLsizei w, GLsizei h) {
    float scaleW = (float)w/width;
    float scaleH = (float)h/height;
    for(auto i = views.begin(); i != views.end(); i++)
        (*i)->reshape(scaleW, scaleH);

    width = w;
    height = h;
}

void Window::processKeys(int key, int x, int y) {
    switch(key) {
    case GLUT_KEY_UP:
        nextObject(true);
        break;
    case GLUT_KEY_DOWN:
        nextObject(false);
        break;
    case GLUT_KEY_RIGHT:
        nextObject(true);
        break;
    case GLUT_KEY_LEFT:
        nextObject(false);
        break;
    }

    for(auto i = listeners.begin(); i != listeners.end(); i++) {
        (*i)->onKeyPressed(key);
    }

    viewParams();
    glutPostRedisplay();
}

void Window::processKeysChar(unsigned char key, int x, int y) {
    switch(key) {
    case 'x':
        cout << "x" << endl;
        keyXEnabled = true;
        keyYEnabled = keyZEnabled= false;
        break;
    case 'y':
        cout << "y" << endl;
        keyYEnabled = true;
        keyXEnabled = keyZEnabled= false;
        break;
    case 'z':
        cout << "z" << endl;
        keyZEnabled = true;
        keyXEnabled = keyYEnabled= false;
        break;
    case 'g':
        cout << "Translate" << endl;
        moovingEnabled = true;
        rotateEnabled = false;
        scaleEnabled = false;
        keyXEnabled = true;
        keyYEnabled = keyZEnabled = false;
        break;
    case 'r':
        cout << "Rotate" << endl;
        moovingEnabled = false;
        rotateEnabled = true;
        scaleEnabled = false;
        keyXEnabled = true;
        keyYEnabled = keyZEnabled = false;
        break;
    case 's':
        cout << "Scale" << endl;
        moovingEnabled = false;
        rotateEnabled = false;
        scaleEnabled = true;
        keyXEnabled = true;
        keyYEnabled = keyZEnabled = false;
        break;
    case 27:
        cout << "View" << endl;
        moovingEnabled = false;
        scaleEnabled = false;
        rotateEnabled = false;
        break;
    case 'c':
        cout << "Change Cam" << endl;
        getAtualView()->nextCamera();
        break;
    case 'v':
        cout << "Change View" << endl;
        nextView();
        break;

    }

    for(auto i = listeners.begin(); i != listeners.end(); i++) {
        (*i)->onKeyCharPressed(key);
    }

    viewParams();
    glutPostRedisplay();
}

void Window::processMouse(int button, int state, int xi, int yi) {
    float x = xi, y = yi;
    transposeChoord(x, y);

    if (button == GLUT_LEFT_BUTTON) {
        if (state == GLUT_DOWN) {
            int pick = picking(xi, yi, 50, 50);
            selectObject(pick);
        }
    }

    if (button == GLUT_LEFT_BUTTON) {

        for(unsigned int i = 0; i < views.size(); i++) {
            if(views[i]->containsClick(x, y))
                view = i;
        }

        if (state == GLUT_DOWN) {
            flagMouseDownRotate = true;
            mouseX = x; mouseY = y;
        } else {
            flagMouseDownRotate = false;
            glutSetCursor(GLUT_CURSOR_RIGHT_ARROW);
        }
    }
    if (button == GLUT_RIGHT_BUTTON) {
        if (state == GLUT_DOWN) {
            flagMouseDownMoved = true;
            mouseX = x; mouseY = y;
        } else {
            flagMouseDownMoved = false;
        }
    }

    switch(button) {
        case 3: {
            getAtualView()->zoom(0.9f);
            break;
        } case 4: {
            getAtualView()->zoom(1.1f);
            break;
        }
    }

    viewParams();
    glutPostRedisplay();
}

void Window::processMuseMotion(int xi, int yi) {
    float x = xi, y = yi;
    transposeChoord(x, y);

    Object3D* object = NULL;
    for(int i = 0; i < objects.size(); i++) {
        if(objects.at(i)->getSelected()) {
            object = objects.at(i);
        }
    }

    if(resizingView) {
        bool movable = true;
        for(auto i = views.begin(); i != views.end(); i++) {
            ViewPort* view = (*i);
            if(view->isMarked())
                movable &= view->isMovableBound(-(mouseX - x), -(mouseY - y));
        }

        if(movable) {
            for(auto i = views.begin(); i != views.end(); i++) {
                ViewPort* view = (*i);
                view->moveBound(-(mouseX - x), -(mouseY - y));
            }
        }

    } else if(moovingEnabled) {
        if(keyXEnabled && object != NULL) {
            object->translate(-(mouseX - x)*0.1, 0, 0);

        } else if(keyYEnabled && object != NULL) {
            object->translate(0, (mouseY - y)*0.1, 0);

        } else if(keyZEnabled && object != NULL) {
            object->translate(0, 0, -(mouseY - y)*0.1);

        }
    } else if(rotateEnabled) {
        if(keyXEnabled && object != NULL) {
            object->rotate(-(mouseY - y)*0.5, 1, 0, 0);

        } else if(keyYEnabled && object != NULL) {
            object->rotate(-(mouseX - x)*0.5, 0, 1, 0);

        } else if(keyZEnabled && object != NULL) {
            object->rotate((mouseY - y)*0.5, 0, 0, 1);

        }
    } else if(scaleEnabled) {
        if(keyXEnabled && object != NULL) {
            object->scale((mouseY - y)*0.1);
        }

    } else if(flagMouseDownRotate) {
        glutSetCursor(GLUT_CURSOR_CYCLE);
        viewAngleY = (mouseX - x)*0.01;
        viewAngleX = -(mouseY - y)*0.01;

        getAtualView()->setAngles(viewAngleY, viewAngleX);

    } else if(flagMouseDownMoved) {
        glutSetCursor(GLUT_CURSOR_CROSSHAIR);
        float mx = (mouseX - x)*1.1;
        float my = -(mouseY - y)*1.1;
        getAtualView()->moveCam(mx, my);
    }

    mouseX = x;
    mouseY = y;

    viewParams();
    glutPostRedisplay();

}

void Window::processMuseMotionPassive(int xi, int yi) {
    float x = xi, y = yi;
    transposeChoord(x, y);

    bool vertical = false;
    bool horizontal = false;
    for(auto i = views.begin(); i != views.end(); i++) {
        ViewPort* view = (*i);

        view->markBound(x, y);

        if(view->inTop(x, y) || view->inBottom(x, y)) {
            horizontal = true;
        }
        if(view->inLeft(x, y) || view->inRight(x, y)) {
            vertical = true;
        }
    }

    resizingView = horizontal || vertical;

    if(vertical && horizontal)
        glutSetCursor(GLUT_CURSOR_CROSSHAIR);
    else if(horizontal)
        glutSetCursor(GLUT_CURSOR_UP_DOWN);
    else if(vertical)
        glutSetCursor(GLUT_CURSOR_LEFT_RIGHT);
    else
        glutSetCursor(GLUT_CURSOR_RIGHT_ARROW);

    mouseX = x;
    mouseY = y;
}

int Window::exec() {
    glutMainLoop();

    return 0;
}


void Window::addObject(Object3D* obj) {
    for(vector<Object3D*>::iterator i = objects.begin(); i != objects.end(); i++)
        (*i)->setSelected(false);
    this->objects.push_back(obj);
    obj->setSelected(true);
}

void Window::addObject(vector<Object3D *> objs) {
    for(vector<Object3D*>::iterator i = objs.begin(); i != objs.end(); i++) {
        (*i)->setSelected(false);
        addObject(*i);
    }
}

void Window::addKeyListener(KeyListener* listener) {
    listeners.push_back(listener);
}

void Window::addView(ViewPort *view) {
    views.insert(views.begin(), view);

    resizeViews();
}

void Window::init() {
    // Especifica que a cor de fundo da janela será branca
    glClearColor(1.0f, 1.0f, 1.0f, 0.0f);

    // Habilita a definição da cor do material a partir da cor corrente
    glEnable(GL_COLOR_MATERIAL);
    //Habilita o uso de iluminação
    glEnable(GL_LIGHTING);
    //Habilita a normalização
    glEnable(GL_NORMALIZE);

    // Habilita o modelo de colorização de Gouraud
    glShadeModel(GL_SMOOTH);

    // Habilita o depth-buffering
    glEnable(GL_DEPTH_TEST);

    // Habilita a luz de número 0
    glEnable(GL_LIGHT0);


    GLfloat luzAmbiente[4]={0.2 ,0.2 ,0.2, 1.0};
    GLfloat luzDifusa[4]={0.7, 0.7, 0.7, 1.0};	     // "cor"
    GLfloat luzEspecular[4]={1.0, 1.0, 1.0, 1.0};// "brilho"
    GLfloat posicaoLuz[4]={lightX, lightY, lightZ, 0.0};

    // Capacidade de brilho do material
    GLfloat especularidade[4]={1.0,1.0,1.0,1.0};
    GLint especMaterial = 100;


    // Define a refletância do material
    glMaterialfv(GL_FRONT,GL_SPECULAR, especularidade);
    // Define a concentração do brilho
    glMateriali(GL_FRONT,GL_SHININESS, especMaterial);

    // Ativa o uso da luz ambiente
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, luzAmbiente);

    // Define os parâmetros da luz de número 0
    glLightfv(GL_LIGHT0, GL_AMBIENT, luzAmbiente);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, luzDifusa );
    glLightfv(GL_LIGHT0, GL_SPECULAR, luzEspecular );
    glLightfv(GL_LIGHT0, GL_POSITION, posicaoLuz );

    glEnable(GL_CULL_FACE);

    viewParams();
    glutPostRedisplay();
}

void Window::viewParams() {
    for(auto i = views.begin(); i != views.end(); i++)
        (*i)->reloadViewParams();
}

void Window::nextObject(bool next) {

    bool containsSelectable = false;
    int size = objects.size();
    for(int i = 0; i < size; i++) {
        containsSelectable |= objects.at(i)->getSelectable();
    }

    if(objects.size() > 0) {
        int size = objects.size();
        for(int i = 0; i < size; i++) {
            Object3D* object = objects.at(i);
            if(object->getSelected()) {

                object->setSelected(false);

                if(next)
                    object = objects.at((i + 1) % size);
                else
                    object = objects.at((i + (size - 1)) % size);

                if(containsSelectable) {
                    object->setSelected(true);

                    if(!object->getSelectable())
                        nextObject(next);
                }

                break;
            }
        }
    }
}

void Window::nextView() {
    ++view %= views.size();
}

void Window::resizeViews() {
    int columns = ceil( sqrt(views.size()) );
    int lines = views.size()/columns;
    float colWidth = (float)width/(columns);
    float lineHeight = (float)height/(columns*lines == views.size()? lines: lines + 1);

    for(int i = 0; i < lines; i++) {
        for(int j = 0; j < columns; j++) {
            float x = j*colWidth;
            float y = i*lineHeight;

            ViewPort* view = views[i*columns + j];
            view->reshape(x, y, colWidth, lineHeight);
        }
    }

    int numIncorrectViews = views.size() -(columns*lines);
    int numCorrectViews = views.size() - numIncorrectViews;

    colWidth = width/(views.size() - numCorrectViews);
    for(unsigned int i = numCorrectViews; i < views.size(); i++) {
        float x = (i - numCorrectViews)*colWidth;
        float y = (columns - 1)*lineHeight;

        ViewPort* view = views[i];
        view->reshape(x, y, colWidth, lineHeight);
    }
}

int Window::processHits(GLint hits, GLuint buffer[]) {
    int i;
    GLuint names, *ptr, minZ,*ptrNames, numberOfNames;

    ptrNames = NULL;

    printf("Hits = %d\n",hits);
    printf("Buffer = ");
    for (i = 0; i < 4*hits; i++) {
        printf("%u ",buffer[i]);
    }
    printf("\n");

    ptr = (GLuint *) buffer;
    minZ = 0xffffffff;
    for (i = 0; i < hits; i++) {
        names = *ptr;
        ptr++;

        if (*ptr < minZ) {
            numberOfNames = names;
            minZ = *ptr;
            if (numberOfNames != 0)
            ptrNames = ptr+2;
        }
        ptr += names+2;
    }

    if (ptrNames == NULL)
    return 0;
    else
        return *ptrNames;
}

int Window::picking(GLint cursorX, GLint cursorY, int w, int h) {
    int BUFSIZE= 512;
    GLuint selectBuf[BUFSIZE];
    displayToSelect(cursorX, cursorY, w, h, BUFSIZE, selectBuf);

    int hits;

    hits = glRenderMode(GL_RENDER);

    if (hits != 0) {
        return processHits(hits,selectBuf);
    } else {
        return 0;
    }
}

void Window::selectObject(int id) {
    Object3D* obj = NULL;
    for(auto i = objects.begin(); i != objects.end(); i++) {
        Object3D* o = (*i);
        o->setSelected(false);
        if(o->getId() == id && o->getSelectable()) {
            obj = o;
        }
    }

    if(obj != NULL) {
        cout << "Selecting: " << id << endl;
        obj->setSelected(true);
    }
}

ViewPort *Window::getAtualView() {
    return views.at(view);
}

void Window::addCamToViews(Camera *cam) {
    for(auto i = views.begin(); i != views.end(); i++) {
        ViewPort* view = (*i);
        view->addCamera(cam);
    }
}

void Window::transposeChoord(float &x, float &y) {
    y = height - y;
}

