#ifndef WINDOW_H
#define WINDOW_H

#include <GL/gl.h>
#include <GL/glut.h>
#include <iostream>
#include <string>
#include <vector>
#include "object3d.h"
#include "keylistener.h"
#include "viewport.h"

using namespace std;

class Window {
public:
    Window(int width, int height, string title, int views = 1);

    void display();
    void displayToSelect(GLint cursorX, GLint cursorY, int w, int h, int BUFSIZE, GLuint selectBuf[]);
    void reshape(GLsizei w, GLsizei h);
    void processKeys(int key, int x, int y);
    void processKeysChar(unsigned char key, int x, int y);
    void processMouse(int button, int state, int x, int y);
    void processMuseMotion(int x, int y);
    void processMuseMotionPassive(int x, int y);

    int exec();

    void addObject(Object3D* obj);
    void addObject(vector<Object3D*> objs);
    void addKeyListener(KeyListener* listener);

    void addView(ViewPort* view);
    ViewPort* getAtualView();
    void addCamToViews(Camera* cam);
    void transposeChoord(float &x, float &y);

private:
    float width, height;

    GLfloat viewAngleX;
    GLfloat viewAngleY;
    GLfloat viewAngleZ;

    float mouseX, mouseY;
    bool flagMouseDownRotate;
    bool flagMouseDownMoved;
    int view;

    GLfloat lightX, lightY, lightZ;

    bool resizingView;
    bool moovingEnabled;
    bool rotateEnabled;
    bool scaleEnabled;
    bool keyXEnabled;
    bool keyYEnabled;
    bool keyZEnabled;

    vector<KeyListener*> listeners;
    vector<Object3D*> objects;
    vector<ViewPort*> views;

    void init();
    void viewParams();
    void nextObject(bool);
    void nextView();
    void resizeViews();
    int processHits(GLint hits, GLuint buffer[]);
    int picking( GLint cursorX, GLint cursorY, int w, int h );
    void selectObject(int id);
};

#endif // WINDOW_H
