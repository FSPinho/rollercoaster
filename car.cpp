#include "car.h"

Car::Car(AbsCurve* track) : Object3D(0, 0, 0) {
    this->track = track;
    trackWidth = track->getNumberOfCurves();
    pos = track->getNumberOfSegments()/4;
    this->cam = NULL;
    this->cam2 = NULL;

    setSelectable(false);
    updatePosition();

}

void Car::onDisplay() {
    glColor3f(1, 1, 1);
    glScalef(0.7f, 0.7f, 1.7f);
    glutSolidCube(1.0);
}

void Car::onDisplaySelected() {
    onDisplay();
}

void Car::onDisplayShadow() {

}

void Car::onKeyPressed(int key) {}

void Car::onKeyCharPressed(int key) {
    switch(key) {
        case 'a':
            goBack();
            break;
        case 'd':
            advance();
            break;
    }
}

void Car::advance() {
    pos += pos < trackWidth? 1: 0;
    updatePosition();
}

void Car::goBack() {
    pos -= pos > 0? 1: 0;
    updatePosition();
}

Matrix<float> Car::getTransposeMatrix(int pos) {
    Vector<float> t = track->getPointAt(pos);

    Vector<float> zl = track->getDerived1(pos)*-1;
    Vector<float> yl = track->getDerived2(pos);
    Vector<float> xl = yl * zl;
    yl = zl * xl;
    xl.normalize();
    yl.normalize();
    zl.normalize();

    Matrix<float> T {
        { xl.getX(), yl.getX(), zl.getX(), t.getX() },
        { xl.getY(), yl.getY(), zl.getY(), t.getY() },
        { xl.getZ(), yl.getZ(), zl.getZ(), t.getZ() },
        { 0, 0, 0, 1 }
    };

    zl = track->getDerived1(pos)*-1;
    yl = track->getDerived2(pos);

    return T;
}

Camera *Car::getCamera() {
    return cam;
}

void Car::setCamera(Camera* cam) {
    this->cam = cam;
    updatePosition();
}

void Car::setCamera2(Camera *cam) {
    this->cam2 = cam;
    updatePosition();
}

void Car::updatePosition() {

    trackWidth = track->getNumberOfCurves();
    Vector<float> point = track->getPointAt(pos);
    Matrix<float> position {
        {1, 0, 0, point.getX()},
        {0, 1, 0, point.getY()},
        {0, 0, 1, point.getZ()},
        {0, 0, 0, 0}
    };

    Matrix<float> toUp1 {
        {1, 0, 0, 0},
        {0, 1, 0, 1},
        {0, 0, 1, 0},
        {0, 0, 0, 1}
    };

    Matrix<float> toUp2 {
        {1, 0, 0, 0},
        {0, 1, 0, 2},
        {0, 0, 1, 0},
        {0, 0, 0, 1}
    };

    Vector<float> point2 = track->getPointAt(pos + 5);
    Matrix<float> toFront {
        {1, 0, 0, point2.getX()},
        {0, 1, 0, point2.getY()},
        {0, 0, 1, point2.getZ()},
        {0, 0, 0, 1}
    };

    Matrix<float> toBack {
        {1, 0, 0, 0},
        {0, 1, 0, 0},
        {0, 0, 1, 2},
        {0, 0, 0, 1}
    };

    Matrix<float> toRight {
        {1, 0, 0, 0},
        {0, 1, 0, 40},
        {0, 0, 1, 0},
        {0, 0, 0, 1}
    };

    Vector<float> zl = track->getDerived1(pos)*-1;
    Vector<float> yl = track->getDerived2(pos);
    Vector<float> xl = yl * zl;
    yl = zl * xl;
    yl.normalize();

    setTransforms({getTransposeMatrix(pos), toUp1});

    if(cam != NULL) {
        cam->setTransforms({getTransposeMatrix(pos), toBack, toUp1});
        cam->setEyePosition({toUp2, getTransposeMatrix(pos >= track->getNumberOfSegments()/4? pos - track->getNumberOfSegments()/4: pos)});
        cam->setCenterPosition({toUp2, getTransposeMatrix(pos)});
        cam->setUpPosition(yl.getX(), yl.getY(), yl.getZ());
    }
    if(cam2 != NULL) {
        cam2->setTransforms({getTransposeMatrix(pos), toBack, toUp1});
        cam2->setEyePosition({toRight, toUp2, getTransposeMatrix(pos >= track->getNumberOfSegments()/4? pos - track->getNumberOfSegments()/4: pos)});
        cam2->setCenterPosition({toUp2, getTransposeMatrix(pos)});
        cam2->setUpPosition(yl.getX(), yl.getY(), yl.getZ());
    }
}
