TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    cubiccurve.cpp \
    object3d.cpp \
    window.cpp \
    sphere3d.cpp \
    beziercurve.cpp \
    abscurve.cpp \
    bsplinecurve.cpp \
    catmullromcurve.cpp \
    car.cpp \
    camera.cpp \
    floor3d.cpp \
    rollercoaster.cpp \
    cube3d.cpp \
    controlobjects.cpp \
    observer.cpp \
    objreader.cpp \
    viewport.cpp

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    vector.h \
    matrix.h \
    cubiccurve.h \
    object3d.h \
    window.h \
    sphere3d.h \
    beziercurve.h \
    matrixes.h \
    abscurve.h \
    bsplinecurve.h \
    catmullromcurve.h \
    car.h \
    keylistener.h \
    camera.h \
    floor3d.h \
    rollercoaster.h \
    cube3d.h \
    controlobjects.h \
    observer.h \
    objreader.h \
    viewport.h

CONFIG += c++11

OTHER_FILES += \
    ICGRollerCoaster.pro.user \
    ICGRollerCoaster.pro.user.18


LIBS += -lGL -lGLU -lglut

DISTFILES += \
    ICGRollerCoaster.pro.user.3.2-pre1 \
    ICGRollerCoaster.pro.user.577f7ff
