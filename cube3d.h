#ifndef CUBE3D_H
#define CUBE3D_H

#include "object3d.h"

class Cube3D : public Object3D {
public:
    Cube3D();
    Cube3D(GLdouble x, GLdouble y, GLdouble z);

    void onDisplay();
    void onDisplaySelected();
    void onDisplayShadow();
};

#endif // CUBE3D_H
