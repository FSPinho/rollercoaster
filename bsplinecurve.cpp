#include "bsplinecurve.h"

BSplineCurve::BSplineCurve(Vector<Vector<float> > controlPoints) : AbsCurve(controlPoints) {
    Matrix<float> m = MATRIX_B_SPLINE;
    m = m*(1.0/6.0);
    setM(m);
}

BSplineCurve::BSplineCurve(Vector<ControlObjects *> controlObjects) : AbsCurve(controlObjects) {
    Matrix<float> m = MATRIX_B_SPLINE;
    m = m*(1.0/6.0);
    setM(m);
}
