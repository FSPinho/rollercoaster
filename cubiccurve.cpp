#include "cubiccurve.h"

CubicCurve::CubicCurve(Vector< Vector<float> > points) : AbsCurve(points) {}

CubicCurve::CubicCurve(Vector<ControlObjects *> controlObjects) : AbsCurve(controlObjects) {}
