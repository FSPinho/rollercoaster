#ifndef BEZIERCURVE_H
#define BEZIERCURVE_H

#include "abscurve.h"
#include "matrixes.h"

class BezierCurve : public AbsCurve {
public:
    BezierCurve(Vector< Vector<float> > controlpoints);
    BezierCurve(Vector<ControlObjects *> controlObjects);

    void onDisplay();
};

#endif // BEZIERCURVE_H
