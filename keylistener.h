#ifndef KEYLISTENER_H
#define KEYLISTENER_H

class KeyListener {
public:
    virtual void onKeyPressed(int key) = 0;
    virtual void onKeyCharPressed(int key) = 0;
};

#endif // KEYLISTENER_H
