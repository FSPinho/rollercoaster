#include "catmullromcurve.h"

CatmullRomCurve::CatmullRomCurve(Vector<Vector<float> > controlPoints) : AbsCurve(controlPoints) {
    Matrix<float> m = MATRIX_CATMULL_ROM;
    m = m*(1.0/2.0);
    setM(m);
}

CatmullRomCurve::CatmullRomCurve(Vector<ControlObjects *> controlObjects) : AbsCurve(controlObjects) {
    Matrix<float> m = MATRIX_CATMULL_ROM;
    m = m*(1.0/2.0);
    setM(m);
}
