#include "floor3d.h"

Floor3D::Floor3D(GLdouble x, GLdouble y, GLdouble z) : Object3D(x, y, z) {
    setSelectable(false);
}

void Floor3D::onDisplay() {
   glPushMatrix();

   glColor3d(.8, .8, .8);
   Object3D::displayCube(0, 0, 0, 50, .1, 50);

   int numberOfLines = 10, distanceBetweenLines = 5;

   glColor3d(1, 1, 1);
   glDisable(GL_LIGHTING);

   for(int x = 0; x < numberOfLines; x++){
       glBegin(GL_LINES);
       glVertex3f(-25, 0.1, x*distanceBetweenLines - 25);
       glVertex3f(25, 0.1, x*distanceBetweenLines - 25);
       glEnd();
   };

   for(int x = 0; x < numberOfLines; x++){
       glBegin(GL_LINES);
       glVertex3f(x*distanceBetweenLines - 25, 0.1, -25);
       glVertex3f(x*distanceBetweenLines - 25, 0.1, 25);
       glEnd();
   };

   glEnable(GL_LIGHTING);

   glPopMatrix();
}

void Floor3D::onDisplaySelected() {

}

void Floor3D::onDisplayShadow() {

}

