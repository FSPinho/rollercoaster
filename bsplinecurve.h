#ifndef BSPLINECURVE_H
#define BSPLINECURVE_H

#include "abscurve.h"
#include "matrixes.h"

class BSplineCurve : public AbsCurve {
public:
    BSplineCurve(Vector<Vector<float> > controlPoints);
    BSplineCurve(Vector<ControlObjects *> controlObjects);

};

#endif // BSPLINECURVE_H
