#include <iostream>
#include <cmath>
#include <GL/gl.h>
#include <GL/glut.h>
#include "rollercoaster.h"
#include "window.h"
#include "floor3d.h"
#include "objreader.h"

using namespace std;

int main(int argc, char* argv[]) {

    glutInit(&argc, argv);

<<<<<<< HEAD
    Window w(640, 480, "Cubic Curve");
=======
    Window w(640, 480, "Cubic Curve", 2);
>>>>>>> ba844a998aaa7a738c828b1d12b03ce9384d1690

    Object3D* floor = new Floor3D(0, 0, 0);
    w.addObject(floor);

<<<<<<< HEAD
    RollerCoaster* rc = new RollerCoaster(0, 30, 0, "rollercoaster.obj");
    //RollerCoaster* rc = new RollerCoaster(0, 50, 0, 10, 10, 50);
=======
    //RollerCoaster* rc = new RollerCoaster(0, 30, 0, "rollercoaster.obj");
    RollerCoaster* rc = new RollerCoaster(0, 100, 0, 20, 10, 50);
>>>>>>> ba844a998aaa7a738c828b1d12b03ce9384d1690
    rc->setPrecision(40);

    w.addObject(rc->getObjects());
    w.addKeyListener(rc->getKeyListener());

    w.addCamToViews(rc->getCamera());
    w.addCamToViews(rc->getCamera2());

    cout << "***********************************************************" << endl;
    cout << "* Type 'g' to move   objects and select the axe (x, y, z) *" << endl;
    cout << "* Type 's' to scale  objects and select the axe (x, y, z) *" << endl;
    cout << "* Type 'r' to rotate objects and select the axe (x, y, z) *" << endl;
    cout << "* Type 'esc' to stop transform                            *" << endl;
    cout << "***********************************************************" << endl;

    w.display();

    return w.exec();
}

